#!/bin/bash

curl -d "ur HDD is started wiping itself now" ntfy.sh/ztXc7fNnUcsxIjOh

DEVICE="/dev/sdb"
PASS=$(tr -cd '[:alnum:]' < /dev/urandom | head -c128)
openssl enc -aes-256-ctr -pass pass:"$PASS" -nosalt </dev/zero | dd obs=64K ibs=4K of=$DEVICE oflag=direct status=progress

curl -d "ur HDD is now fully wiped😀" ntfy.sh/ztXc7fNnUcsxIjOh
