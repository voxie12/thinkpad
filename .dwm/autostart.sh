xset r rate 300 50 &
xset s off -dpms &
xwallpaper --zoom ~/pic/wall/wallhaven-x8wrel_1920x1080.png &
lxsession &
dunst &
udiskie &
xbacklight -set 40 &
setxkbmap gb &
killall slstatus
slstatus &
sxhkd &
unclutter --timeout 3 &
xcompmgr &
