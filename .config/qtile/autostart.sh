#!/bin/bash

xset r rate 300 50 &
xset s off -dpms &
xwallpaper --zoom ~/pic/wall/wallhaven-x8wrel_1920x1080.png &
lxsession &
dunst &
udiskie &
xbacklight -set 40 &
setxkbmap gb &
unclutter &
picom  -f &
killall redshift
redshift &
