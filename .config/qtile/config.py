import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown, KeyChord
from libqtile.lazy import lazy
#from libqtile.utils import guess_terminal
from typing import List # noqa: F401
from libqtile.widget import spacer
import colors

mod = "mod4"
terminal = "alacritty"
browser = "firefox"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod],"space", lazy.spawn("dmenu_run"), desc='dmenu'),

    # custom
    Key([mod],"w", lazy.spawn("firefox"), desc='open firefox'),
    Key([mod],"t", lazy.spawn("telegram-desktop"), desc='open telegram'),
    Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle"), desc='dmenu'),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 5%-"), desc='dmenu'),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 5%+"), desc='dmenu'),
    Key([], "XF86AudioMicMute", lazy.spawn("amixer set Capture toggle"), desc='dmenu'),

]

#groups = [Group(i) for i in "123456789"]
groups = [
    Group(
        '1',
        label="一",
        matches=[
            Match(wm_class=["Alacritty", "kitty"]),
            ],
        layout="monadtall"
    ),
    Group('2', label="二", layout="max", matches=[Match(wm_class=["firefox", "brave"])]),
    Group('3', label="三", layout="monadtall", matches=[Match(wm_class=["telegram-desktop"])]),
    Group('4', label="四", layout="monadtall", matches=[Match(wm_class=["qBittorrent"])]),
    Group('5', label="五", layout="monadtall", matches=[Match(wm_class=["discord"])]), 
    Group('6', label="六", layout="monadtall"), 
    Group('7', label="七", layout="monadtall"), 
    Group('8', label="八", layout="monadtall"), 
    Group('9', label="九", layout="monadtall"), 
    Group('0', label="十", layout="max", matches=[Match(wm_class=["mpv"])])
]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

colors = colors.GruvboxMaterial

layout_theme = {"border_width": 2,
                "margin": 10,
                "border_focus": "#ea6962",
                "border_normal": colors[0]
                }


layouts = [
    layout.Max(
         border_width = 0,
         margin = 0,
        ),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Zoomy(**layout_theme),
]


widget_defaults = dict(
    font="Hack Nerd Font",
    fontsize=12,
    padding=5,
    background=colors[0],
    #foreground=colors[1],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(
                    #fontsize = 11,
                    #margin_y = 3,
                    #margin_x = 4,
                    #padding_y = 2,
                    #padding_x = 3,
                    #borderwidth = 3,
                    active = colors[8],
                    inactive = colors[1],
                    rounded = False,
                    highlight_color = colors[2],
                    highlight_method = "text",
                    this_current_screen_border = colors[7],
                    this_screen_border = colors [4],
                    other_current_screen_border = colors[7],
                    other_screen_border = colors[4],
                    ),
                widget.Prompt(),
                    widget.Spacer(
                    length = bar.STRETCH
                    ),
                widget.CheckUpdates(
                    distro = "Arch_checkupdates",
                    update_interval = 1800,
                    display_format = "[updates: {updates}]",
                    foreground = colors[4],
                    #background = colors["bg"],
                    colour_have_updates = colors[4],
                    #colour_no_updates = colors["red"],
                    ),
                #widget.TextBox("[ThinkPad T490]", foreground=colors[3]),
                widget.Net(
                    prefix='M',
                    format=' {down:.0f}',
                    interface="wlp0s20f3",
                    foreground = colors[5],
                    fmt='[{}]'
                    ),
                widget.Memory(
                    foreground = colors[8],
                    format = '{MemUsed:.0f}{mm}',
                    fmt = '[{}]',
                 ),
                #widget.Sep(),
                widget.Battery(
                    format="[{percent:2.0%}]",
                    foreground = colors[4]
                    ),
                #widget.Sep(),
                widget.Clock(
                    format="[%d/%m/%y - %R]",
                    foreground = colors[6]
                    ),
            ],
            25,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
            #margin = [0, 10, 10, 10], # Draw top and bottom borders   [ top, right, bottom, left ]

        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])
