# ThinkPad

Dotfile for My ThinkPad T490.

## Install:
```bash
sudo pacman -S yadm
yadm clone https://gitlab.com/voxie12/thinkpad

```
## Dependance:
```bash
sudo pacman -S xwallpaper udiskie zsh unclutter \
sxhkd xcompmgr noto-fonts noto-fonts-cjk
```

## AUR:
```bash
paru -S zsh-fast-syntax-highlighting
paru -S auto-cpufreq
paru -S nerd-fonts-hack
paru -S qtile-extras
```

